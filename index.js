import { Todo } from './todo.js';


function App() {

this.todoCtrl = new TodoController()

    //DOM Elements
this.elNewTodoTitle = document.getElementById('new-todo-title')
this.elBtnAddTodo = document.getElementById('btn-add-todo')
this.elTodoList = document.getElementById('todo-list')

this.init = function() {
    //Init event listerners
    this.elBtnAddTodo.addEventListener('click', this.handleAddTodoClick.bind(this))
    this.render()
}

//Event Handlers
this.handleAddTodoClick = function() {
    //Get the new todo value
    const newTodo = this.elNewTodoTitle.value.trim();
    // Add it to the array
    this.todoCtrl.addTodo(newTodo);
    //Re-render our HTML
    this.render()
}
 this.render = function()  {
    this.elTodoList.innerHTML = ''
    this.todoCtrl.createTodoItems()
    .forEach((todoIem) => {
        this.elTodoList.appendChild(todoIem)
        })
    }
}

function TodoController() {

    this.todos = [
        new Todo(1, 'My first todo 🥇' ),
        new Todo(2,'Have lunch with M 🥰' ), 
        new Todo(3, 'Pinpoint with Martin ‼️ ✨')
    ]

    this.addTodo = function(title) {
        const id = this.todos[this.todos.length-1].id + 1
        this.todos.push(new Todo(id, title))
        
    }

    this.createTodoItems = function() {
        const elTodoItems = this.todos.map(function(todo) {
           const elTodoItem = document.createElement('li')
           elTodoItem.innerText = todo.title;
           return elTodoItem
           })
           return elTodoItems
       }
}


new App().init();